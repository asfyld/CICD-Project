
Final project of the EKPA eLearning Academy DevOps Engineering program.

Aggelos Loudianos

Description:

For the successful completion of the educational program, you are required to prepare this assignment, which assesses the majority of the knowledge you have acquired during its duration. Use an open-source application of your choice from GitHub.

-Create the corresponding project in Git and synchronize it with the remote repository on GitHub/GitLab.

-Create the relevant Dockerfile and build the application's image. At this point, your application should be able to run as a Docker Container.

-Create your own deployment server, either on your local machine or using a platform like Digital Ocean

-Utilize your preferred platform (e.g., GitHub Actions, Gitlab, Jenkins) and set up the CI/CD Pipeline. At a minimum, the pipeline should include 3 stages: Testing, Build Image, Deploy

Ubuntu Server AWS Frankfurt
3.72.106.20:5000 (Credits expired)

